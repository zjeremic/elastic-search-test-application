package com.testweb.es;

 
 

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

 
import org.testng.annotations.Test;

import com.testweb.es.ESIndexer;

public class ESIndexerTest {
  
  @Test
  public void testSimpleAttachmentTest(){
	  ESIndexer indexer=new ESIndexer();
	  List<String> links=new LinkedList<String>();
	  links.add("http://en.wikipedia.org/wiki/Hibernate_%28Java%29");
	  links.add("http://en.wikipedia.org/wiki/Object-relational_mapping");
	  links.add("http://en.wikipedia.org/wiki/Object_database");
	  links.add("http://en.wikipedia.org/wiki/Object-oriented_programming");
	  links.add("http://en.wikipedia.org/wiki/Europe");
	  links.add("http://en.wikipedia.org/wiki/Arctic_Ocean");
	  links.add("http://software.intel.com/en-us/blogs/2008/08/22/flaws-of-object-oriented-modeling/");
	  links.add("http://en.wikipedia.org/wiki/Object-Oriented_Software_Construction");
	  links.add("http://mitpress.mit.edu/sicp/");
	  links.add("http://mitpress.mit.edu/sicp/course.html");
	  links.add("http://en.wikipedia.org/wiki/Temperature");
	  links.add("http://en.wikipedia.org/wiki/Thermometer");
	  links.add("http://en.wikipedia.org/wiki/Mercury-in-glass_thermometer");
	  links.add("http://en.wikipedia.org/wiki/Daniel_Gabriel_Fahrenheit");
	  links.add("http://en.wikipedia.org/wiki/Physicist");
	  links.add("http://en.wikipedia.org/wiki/Particle_physics");
	  links.add("http://en.wikipedia.org/wiki/Quantum_field_theory");
	  try {
		  for(String link:links){
			  System.out.println("Adding link:"+link);
			  indexer.indexHTMLPage(link);
		  }
		 Thread.sleep(500000);
		
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
  }

  @Test
  public void indexDirectoryTest(){
	  String path="/home/zoran/Desktop/documents_corpus";
	  ESIndexer indexer=new ESIndexer();
	  indexer.indexDirectory(new File(path));
	  
	  try {
		Thread.sleep(500000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
  }
}
