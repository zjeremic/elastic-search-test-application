package com.testweb.es;

import static org.elasticsearch.client.Requests.indexRequest;
import static org.elasticsearch.client.Requests.putMappingRequest;
import static org.elasticsearch.client.Requests.refreshRequest;
import static org.elasticsearch.common.io.Streams.copyToStringFromClasspath;
import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;
import static org.elasticsearch.node.NodeBuilder.nodeBuilder;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Date;

import org.elasticsearch.ElasticSearchException;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.node.Node;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.highlight.HighlightField;
 
 
 

public class ESIndexer { 
	String indexName="documents";
	String indexTypeWebPage="webpages";
	String indexTypeFile="files";
	String fields="file";
 
	private static Client getESClient() {
		//ElasticSearchConfig elasticSearchConfig=Settings.getInstance().config.elasticSearch;
		org.elasticsearch.common.settings.Settings settings = ImmutableSettings
				.settingsBuilder().put("cluster.name", "elasticsearch").build();
		Client client = new TransportClient(settings)
				.addTransportAddress(new InetSocketTransportAddress(
						"localhost", 9300));
		return client;
	}
 
	public void indexHTMLPage(final String link){
		new Thread(new Runnable() {
			@Override
			public void run() {
				
				Client client=getESClient();
					try {
					    String mapping = copyToStringFromClasspath("/com/inextweb/es/documents-mapping.json");
				      URL url=new URL(link);
					     InputStream inputStream= url.openStream();
					    // byte[] html = copyToBytesFromClasspath("/com/inextweb/es/testXHTML.html");
					      byte[] html=org.elasticsearch.common.io.Streams.copyToByteArray(inputStream);
					client.admin().indices().putMapping(putMappingRequest(indexName).type(indexTypeWebPage).source(mapping)).actionGet();
			 	  // byte[] html=org.elasticsearch.common.io.Streams.copyToByteArray(input);
			 		client.index(indexRequest(indexName).type(indexTypeWebPage)
			                .source(jsonBuilder()
			                		.startObject()
			                			//.field("content", finput)
			                			.field("file",html)
			                			.field("title","title for "+link)
			                			.field("description","title for "+link)
			                			.field("contentType","WebPage")
			                			.field("dateCreated",new Date())
			                			.field("url",link)
			                			.endObject()))
			               		.actionGet();
			        client.admin().indices().refresh(refreshRequest()).actionGet();
				//	System.out.println("finished index for:"+richContent.getId()+" user title:"+richContent.getTitle());
			 
					//TODO just testing input
				
				       
				//	b64stream.close();
					client.close();
				} catch (ElasticSearchException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();
	}
	
	  public void indexingWebPage(String link) throws Exception {
	      String mapping = copyToStringFromClasspath("/com/testweb/es/webpages-mapping.json");
	      final int CONTENT_LENGTH_LIMIT = -1;
	      URL url=new URL(link);
	     InputStream inputStream= url.openStream();
	    // byte[] html = copyToBytesFromClasspath("/com/inextweb/es/testXHTML.html");
	      byte[] html=org.elasticsearch.common.io.Streams.copyToByteArray(inputStream);
	   
      Client client=getESClient();
	       client.admin().indices().putMapping(putMappingRequest(indexName).type(indexTypeWebPage).source(mapping)).actionGet();
	       
	      client.index(indexRequest(indexName).type(indexTypeWebPage)
	              .source(jsonBuilder()
	            		  .startObject()
              			.field("file",html)
              			 .field("title","Web page title goes here")
              			.field("description","Some user added description comes here")
              			.field("contentType","Web page")
              			.field("dateCreated",new Date())
              			.field("url",url.toString())
              			.endObject()))
	             .actionGet();
	       client.admin().indices().refresh(refreshRequest()).actionGet();
	       client.close();
 
	  }
	  public void indexDirectory(final File file)  {
			
			new Thread(new Runnable() {
				@Override
				public void run() {
					System.out.println("indexing file started:" + file.getAbsolutePath());
					if (file.canRead()) {
						if (file.isDirectory()) {
							String[] files = file.list();

							if (files != null) {
								for (int i = 0; i < files.length; i++) {
									try {
										indexDirectory(new File(file, files[i]));
									} catch (Exception e) {
										System.out.println("Exception:"+e.getLocalizedMessage());
									}
								}
							}
						} else {
							System.out.println("Indexing " + file+file.getAbsolutePath());
							 try {
								 indexFile(file);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}
			}).start();
		}
	  public	void indexFile(File file) throws IOException{
		 
		  String mapping = copyToStringFromClasspath("/com/testweb/es/documents-mapping.json");
			 byte[] txt =org.elasticsearch.common.io.Streams.copyToByteArray(file);
				Client client=getESClient();
				client.admin().indices().putMapping(putMappingRequest(indexName).type(indexTypeFile).source(mapping)).actionGet();
				client.index(indexRequest(indexName).type(indexTypeFile)
		                .source(jsonBuilder()
		                		.startObject()
		                			.field("file",txt)
		                			.field("title","title for file "+file.getAbsolutePath())
		                			.field("description","description for file "+file.getAbsolutePath())
		                			.field("contentType","FILE")
		                			.field("dateCreated",new Date())
		                			.field("url",file.getAbsolutePath())
		                			.endObject()))
		               		.actionGet();
		        client.admin().indices().refresh(refreshRequest()).actionGet();
				System.out.println("finished index for:"+file.getAbsolutePath());
				
			
				
				
				
				client.close();
		}
}
